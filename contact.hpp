#ifndef CONTACT_HPP
#define CONTACT_HPP

#include "commons.hpp"

class Contact
{
    public:
        Contact();
        Contact(const Contact&);
        ~Contact();
        void operator=(const Contact&);
        unsigned int GetidContact() const { return idContact; }
        void SetidContact(unsigned int val) { idContact = val; }
        char* Getnom() const { return nom; }
        void Setnom(const char* val);
        char* Getprenom() const { return prenom; }
        void Setprenom(const char* val);
        char* Getsexe() const { return sexe; }
        void Setsexe(const char* val);
        virtual string infos() const;

    protected:

    private:
        unsigned int idContact;
        char* nom;
        char* prenom;
        char* sexe;
};

extern ostream& operator<<(ostream& stream, Contact& c);

#endif // CONTACT_HPP
