#include "sqlite_io.hpp"
Db::Db():path(""),dbo(NULL), stmt(NULL), query_type(SELECT)
{

}

Db::~Db()
{
    if(this->dbo != NULL)
    {
        this->disconnect();
    }
    if(this->stmt != NULL)
    {
        this->finalize_query();
    }
}

bool Db::connect(string path)
{
    if(sqlite3_open(path.c_str(), &this->dbo) == SQLITE_OK)
    {
        this->path = path;
        return true;
    }
    return false;
}

void Db::disconnect()
{
   sqlite3_close(this->dbo);
   this->dbo = NULL;
}

bool Db::init_query(Query_Type type, Db_template& o, const char* condition)
{
    if(this->stmt != NULL) this->finalize_query();
    this->query_type = type;
    string query;
    switch(type)
    {
        case Query_Type::SELECT: query = build_query_select(o, condition);break;
        case Query_Type::INSERT: query = build_query_insert(o); break;
        case Query_Type::DELETE: query = build_query_delete(o, condition); break;
        case Query_Type::UPDATE: query = build_query_update(o, condition); break;
        default: throw logic_error("Not implemented");
    }
    int result = sqlite3_prepare_v2(dbo, query.c_str(), -1, &stmt, NULL);
    return  result == SQLITE_OK;
}

bool Db::get_next(Db_template& o)
{
    if(stmt == NULL) this->init_query(Query_Type::SELECT, o, NULL);
    if(this->query_type != SELECT) return false;
    bool result = sqlite3_step(stmt) == SQLITE_ROW;
    o.clean_model();
    if(result)
    {
        o.read_from_stmt(stmt);
    }
    else
    {
        finalize_query();
    }
    return result;
}

bool Db::exec_insert(Db_template& o)
{
    if(stmt == NULL)
    {
        this->init_query(INSERT, o, NULL);
    }
    else
    {
        sqlite3_reset(stmt);
    }
    if(this->query_type != INSERT) return false;
    o.bind_to_stmt(stmt, false);
    bool result = sqlite3_step(stmt) == SQLITE_DONE;
    return result;
}

bool Db::exec_update(Db_template& o)
{
    if(stmt == NULL) throw logic_error("Requête non initialisée");
    if(this->query_type != UPDATE) return false;
    o.bind_to_stmt(stmt, true);
    bool result = sqlite3_step(stmt) == SQLITE_DONE;
    finalize_query();
    return result;
}

bool Db::exec_delete()
{
    if(stmt == NULL) throw logic_error("Requête non initialisée");
    if(this->query_type != DELETE) return false;
    bool result = sqlite3_step(stmt) == SQLITE_DONE;
    finalize_query();
    return result;
}


void Db::finalize_query()
{
    sqlite3_finalize(stmt);
    stmt = NULL;
    this->query_type = SELECT;
}

bool Db::execute_query(const char* query)
{
    if(this->stmt != NULL) this->finalize_query();
    bool result = sqlite3_prepare_v2(dbo, query, -1, &stmt, NULL) == SQLITE_OK;
    if(result)
    {
        result = sqlite3_step(stmt) == SQLITE_DONE;
    } 
    this->finalize_query();
    return result;
}

bool Db::begin_transaction()
{
    return execute_query("BEGIN TRANSACTION;");
}

bool Db::commit_transaction()
{
    return execute_query("COMMIT;");
}

bool Db::rollback_transaction()
{
    return execute_query("ROLLBACK;");
}

