#include "date.hpp"

Date::Date()
{
    //ctor
}

Date::~Date()
{
    //dtor
}

string Date::infos() const
{
    stringstream ss;
    ss << this->jour  << "/" << this->mois << "/" << this->annee;
    return ss.str();
}

ostream& operator<<(ostream& stream, Date& d)
{
    stream << d.infos();
    return stream;
}

