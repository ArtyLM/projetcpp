Projet C++.

Membres : Mathias, Pierrick, Arthur

Création d'un annuaire qui permettra à l'utilisateur de chercher, renseigner et supprimer un contact.

## Guide
L'utilisateur peut au choix:
 - Afficher la liste de la base de données
 - Afficher la liste en filtrant suivant le Nom ou la Ville
 - Créer un nouveau contact
 - Supprimer un contact existant

 Toutes les opérations sont effectuées directement sur la base de données dbContacts.db

 Un service de mise à jour automatique démarre au lancement de l'application, il scrute toutes les 10 secondes les fichiers `newprivates.csv` et `newprofs.csv` du dossier `ressources`.

 ## Configuration

Le fichier `commmons.hpp` permet de configurer les chemins des bases de données et des fichiers à surveiller. Il permet également de configurer le cycle de mise à jour automatique:

 - `dbPath` définit le chemin de la base de données dbContacts.db  (valeur: `../dbContacts.db`)
 - `private_path` définit le chemin du fichier d'échange des contacts privés (valeur: `../ressources/newprivates.csv`)
 - `pro_path` définit le chemin du fichier d'échange des contacts pros (valeur: `../ressources/newprofs.csv`)
 - `UPDATE_DELAY_SECONDS` définit en secondes le cycle de mise à jours (valeur: 10);

 Le commit taggé 2.0 est fonctionnel et couvre les exercices de 1 à 4.


