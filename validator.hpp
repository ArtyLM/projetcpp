#pragma once

#include <iostream>
#include <string>
#include <limits>

using namespace std;

class Validator
{
    public:
        char getValidChar(const char* validChar, int nbChars);
        string getValidString_Length(int maxLength);
        string getValidString_Contain_Char(char c);
        int getNumber();
        int getNumber(int valMin, int valMax);

};