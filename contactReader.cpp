#include "contactReader.hpp"

#define CONTACT_PRIVE_NB_PROP 9
#define CONTATC_PRO_NB_PROP 10

void handle_carriage_return(vector<string>& row)
{
    string last_w = row[row.size()-1];
    if (!last_w.empty() && last_w[last_w.size() - 1] == '\r')
    {
       row[row.size()-1] = last_w.erase(last_w.size() - 1);
    }
}

int get_value_or_default(string value, int defaultValue)
{
    if(value.empty())
    {
        return defaultValue;
    }
    else
    {
        return stoi(value);
    }
}

const char* get_value_or_default(string value, const char* defaultValue)
{
    if(value.empty())
    {
        return defaultValue;
    }
    return value.c_str();
}

Date get_value_or_default(string value, Date defaultValue)
{
    if(value.empty())
    {
        return defaultValue;
    }
    Date d;
    vector<string> row;
    string word;
    stringstream str(value);
    while(getline(str, word, '/'))
    {
        row.push_back(word);
    }
    if(row.size()==3)
    {
        d.Setjour(stoi(row[0]));
        d.Setmois(stoi(row[1]));
        d.Setannee(stoi(row[2]));
        return d;
    }
    return defaultValue;

}

int line_to_Contact_commons(Contact& c, vector<string> l)
{
    int i=0;
    c.SetidContact(get_value_or_default(l[i++], -1));
    c.Setnom(get_value_or_default(l[i++], (char*)NULL));
    c.Setprenom(get_value_or_default(l[i++], (char*)NULL));
    c.Setsexe(get_value_or_default(l[i++], (char*)NULL));
    return i;
}

void line_to_Contact(ContactPrive& c, vector<string>& l)
{
    int i = line_to_Contact_commons(c, l);
    Adresse a;
    a.Setrue(get_value_or_default(l[i++], (char*)NULL));
    a.Setcomplement(get_value_or_default(l[i++], (char*)NULL));
    a.Setcp(get_value_or_default(l[i++], (char*)NULL));
    a.Setville(get_value_or_default(l[i++], (char*)NULL));
    c.Setadresse(a);
    c.SetdtNaissance(get_value_or_default(l[i++], Date()));
}

void line_to_Contact(ContactPro& c, vector<string>& l)
{
    int i= line_to_Contact_commons(c, l);
    c.Setentreprise(get_value_or_default(l[i++], (char*)NULL));
    Adresse a;
    a.Setrue(get_value_or_default(l[i++], (char*)NULL));
    a.Setcomplement(get_value_or_default(l[i++], (char*)NULL));
    a.Setcp(get_value_or_default(l[i++], (char*)NULL));
    a.Setville(get_value_or_default(l[i++], (char*)NULL));
    c.SetadresseEntreprise(a);
    c.Setmail(get_value_or_default(l[i++], (char*)NULL));
}

bool ContactReader::openReader(string path, bool skip_header)
{
    if(this->reader.is_open())
    {
        this->reader.close();
    }
    this->reader = ifstream(path);
    bool result = this->reader.is_open();
    if(result && skip_header)
    {
        string line;
        getline(this->reader, line);
    }
    return result;
}

bool ContactReader::get_next(ContactPrive* c)
{
    vector<string> row;
    string line;
    string word;
    bool result = false;
    while(getline(this->reader, line))
    {
        row.clear();
        stringstream str(line);
        while(getline(str, word, ';'))
        {
            row.push_back(word);
        }
        if(row.size()!=CONTACT_PRIVE_NB_PROP)
        {
            continue;;
        }
        handle_carriage_return(row);
        line_to_Contact(*c, row);
        result = true;
    }
    if(!result)
    {
        this->reader.close();
    }

    return result;
}

bool ContactReader:: get_next(ContactPro* c)
{
    vector<string> row;
    string line;
    string word;
    bool result = false;
    while(getline(this->reader, line))
    {
        row.clear();
        stringstream str(line);
        while(getline(str, word, ';'))
        {
            row.push_back(word);
        }
        if(row.size()!=CONTATC_PRO_NB_PROP)
        {
            continue;
        } 
        handle_carriage_return(row);
        line_to_Contact(*c, row);
        result = true;
    }
    if(!result)
    {
        this->reader.close();
    }

    return result;
}

vector<ContactPrive> ContactReader::read_all_privates()
{
    vector<ContactPrive> result;
    ContactPrive c;
    while(get_next(&c))
    {
        result.push_back(c);
    }

    return result;
}

vector<ContactPro> ContactReader:: read_all_pros()
{
    vector<ContactPro> result;
    ContactPro c;
    while(get_next(&c))
    {
        result.push_back(c);
    }

    return result;
}