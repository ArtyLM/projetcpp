#ifndef TEST_H
#define TEST_H

#include <iostream>
using namespace std;

class test
{
    public:
        test();
        ~test();

        int Getage() { return age; }
        void Setage(int val) { age = val; }
        string Getnom() { return nom; }
        void Setnom(string val) { nom = val; }
        string Getprenom() { return prenom; }
        void Setprenom(string val) { prenom = val; }

    protected:

    private:
        int age;
        string nom;
        string prenom;
};

#endif // TEST_H
