#ifndef CONTACTPRIVE_HPP
#define CONTACTPRIVE_HPP

#include "contact.hpp"
#include "adresse.hpp"
#include "date.hpp"


class ContactPrive : public Contact
{
    public:
        ContactPrive();
        ~ContactPrive();

        Adresse Getadresse() const { return adresse; }
        void Setadresse(Adresse val) { adresse = val; }
        Date GetdtNaissance() const { return dtNaissance; }
        void SetdtNaissance(Date val) { dtNaissance = val; }
        string infos() const override;

    protected:

    private:
        Adresse adresse;
        Date dtNaissance;
};

#endif // CONTACTPRIVE_HPP
