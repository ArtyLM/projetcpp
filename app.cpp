#include "app.h"


app::app()
{

}

void app::init()
{
    updater.start();
    cout << endl << "Bienvenue dans l'application de gestion des contacts!" << endl << endl;
    afficheMenu();
    selection_action();
}

void setCommonProperties(Contact& c)
{
    cout << "Prénom: " ;
    cin.ignore();
    string rep = validator.getValidString_Length(30);
    c.Setprenom(rep.c_str());
    cout << "Nom: " ;
    rep = validator.getValidString_Length(30);
    c.Setnom(rep.c_str());
    cout << "Sexe (F/M): ";
    char sexe = validator.getValidChar("fm", 2);
    char s[]={sexe};
    c.Setsexe(&s[0]);
    cin.ignore();
}

void build_adresse(Adresse& a)
{
    string rep;
    cout << "Rue: ";
    rep = validator.getValidString_Length(250);
    a.Setrue(rep.c_str());
    cout << "Complément d'adresse: " ;
    rep = validator.getValidString_Length(250);
    a.Setcomplement(rep.empty() ? NULL : rep.c_str());
    cout << "Code postal: ";
    rep = validator.getValidString_Length(5);
    a.Setcp(rep.c_str());
    cout << "Ville: ";
    rep = validator.getValidString_Length(100);
    a.Setville(rep.c_str());
}

void build_date(Date& d)
{
    int rep;
    cout << "Jour: ";
    rep = validator.getNumber(1,31);
    d.Setjour(rep);
    cout << "Mois: ";
    rep = validator.getNumber(1,12);
    d.Setmois(rep);
    cout << "Année: ";
    rep = validator.getNumber();
    d.Setannee(rep);
}

ContactPrive* create_contact_prive()
{
    ContactPrive* c = new ContactPrive();
    setCommonProperties(*c);
    Adresse a;
    cout << "Addresse du domicile" << endl;
    build_adresse(a);
    c->Setadresse(a);
    Date d;
    cout << "Date de naissance" << endl;
    build_date(d);
    c->SetdtNaissance(d);
    return c;
}

ContactPro* create_contact_pro()
{
    ContactPro* cp = new ContactPro();
    setCommonProperties(*cp);
    string rep;
    cout << "Addresse mail: " ;
    rep = validator.getValidString_Contain_Char('@');
    cp->Setmail(rep.c_str());
    cout << "Entreprise: ";
    rep = validator.getValidString_Length(50);
    cp->Setentreprise(rep.c_str());
    Adresse a;
    cout << "Addresse de l'entreprise" << endl;
    build_adresse(a);
    cp->SetadresseEntreprise(a);
    return cp;
}

void app::ajouterContact()
{
    char choix;
    cout << "Créer un contact privé ou professionnel (i/p)?";
    choix = validator.getValidChar("ip",2);
    Contact* c;    
    if(towlower(choix)=='i')
    {
        c = create_contact_prive();
    }
    else
    {
        c = create_contact_pro();
    }
    cout << *c << endl;
    Db db;
    if(db.connect(dbPath))
    {
        Db_template_contact contact;
        if(db.init_query(INSERT, contact, NULL))
        {
            contact.update_DbInstance(c);
            if(db.exec_insert(contact))
            {
                cout << "Le contact a été créé avec succès." << endl;
            }
            else
            {
                cout << "Erreur lors de la création du contact!!" << endl;
            }
        }
        db.finalize_query();
        db.disconnect();
    }
    free(c);
}

string build_ville_condition()
{
    string choix;
    cout << "Nom de la ville: ";
    choix = validator.getValidString_Length(250);
    stringstream ss;
    ss << "Ville like '" << choix << "'";
    return ss.str();
}

string build_nom_condition()
{
    string choix;
    cout << "Nom du contact: ";
    choix = validator.getValidString_Length(250);
    stringstream ss;
    ss << "Nom like '" << choix << "'";
    return ss.str();
}

void app::rechercheContact()
{   
    cout << "Recherche par nom ou par ville (n/v)?";
    char type= validator.getValidChar("vn", 2);
    cin.ignore();
    string condition = tolower(type)=='v' ? build_ville_condition() : build_nom_condition();
    this->listeContact(condition.c_str());
}

void app::supprimerContact()
{
    int id;
    cout << "Quel est l'identifiant du contact à supprimer? ";
    id = validator.getNumber();
    stringstream ss;
    ss << "IdContact == " << id;
    string condition = ss.str();
    Db db;
    if(db.connect(dbPath))
    {
        Db_template_contact contact;
        if(db.init_query(DELETE, contact, condition.c_str()))
        {
            if(db.exec_delete())
            {
                cout << "Le contact " << id << " a bien été supprimé." << endl;
            }
            else
            {
                cout << "Le contact " << id << " n'existe pas." << endl;
            }
        }
        db.finalize_query();
        db.disconnect();
    }
}

void app::listeContact(const char* condition)
{
    Db db;
    if(db.connect(dbPath))
    {
        Db_template_contact contact;
        if (db.init_query(SELECT, contact, condition))
        {
            cout << "Liste des contacts";
            if(condition != NULL)
            {
                cout << "(filtrée)";
            }
            cout << ":" << endl;
            int count = 0;
            while(db.get_next(contact))
            {
                cout << "----------------" << endl;
                cout << "Id: " << contact.getDbInstance().getIdContact() << endl;
                cout << contact.getModel().infos() << endl;
                count++;
            }

            if(count == 0)
            {
                if(condition == NULL)
                {
                    cout << "La liste est vide." << endl;
                }
                else
                {
                    cout << "La recherche n'a donné aucun résultat." << endl;
                }
            }
        }
        db.disconnect();
    }
}

void app::quitter()
{
    updater.stop();
    cout << "Merci d'avoir utilisé l'application.  À bientôt!" << endl;
}

void app::afficheMenu()
{
    cout << "Gestionnaire de contacts:" << endl;
    cout << "-------------------------" << endl;
    cout << "(1) - Afficher les contacts" << endl;
    cout << "(2) - Rechercher des contacts" << endl;
    cout << "(3) - Ajouter un contact" << endl;
    cout << "(4) - Supprimer un contact" << endl;
    cout << "(q) Quitter l'application" << endl;
}


void app::selection_action()
{
    char answer;
    cout << endl << "Que voulez vous faire (tapez '?' pour de l'aide)? " ;
    cin >> answer;

    switch(answer)
    {
        case '1':
            this->listeContact(NULL);
            break;

        case '2':
            this->rechercheContact();
            break;

        case '3':
            this->ajouterContact();
            break;

        case '4':
            this->supprimerContact();
            break;
        
        case 'q':
        case 'Q':
            this->quitter();
            return;

        case '?':
            this->afficheMenu();
            break;

        default:
            cout << "Erreur de saisie!" << endl;
            afficheMenu();
            break;
    }
    this->selection_action();
}


