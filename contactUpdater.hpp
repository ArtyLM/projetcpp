#pragma once
#include "contactReader.hpp"
#include "commons.hpp"
#include "sqlite_io.hpp"
#include <pthread.h>
#include <unistd.h>

class ContactUpdater
{
    public:
        ContactUpdater(){}
        void start();
        void stop();
    private:
        bool update_contacts_pros();
        bool update_contacts_prives();
        ContactReader reader;
        pthread_t thread;
        friend void* update_contacts(void*);
};

void* update_contacts(void*);
