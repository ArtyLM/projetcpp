#include "contactUpdater.hpp"

bool ContactUpdater::update_contacts_pros()
{
    bool result = false;
    if(does_file_exist(pro_path) && reader.openReader(pro_path, true))
    {
        Db db;
        if(db.connect(dbPath))
        {
            Db_template_contact contact;
            if(db.init_query(Query_Type::INSERT, contact))
            {
                ContactPro c;
                bool error = false;               
                if(db.begin_transaction())
                {
                    while(reader.get_next(&c))
                    {
                        contact.update_DbInstance(&c);
                        error = !db.exec_insert(contact);
                        if(error)
                        {
                            break;
                        }
                    }
                    if(error)
                    {
                        db.rollback_transaction();
                    }
                    else
                    {
                        db.commit_transaction();
                        result = true;
                    }
                }
            }
        }
        if(result)
        {
            remove(pro_path.c_str());
        }
    }
    return result;
}

bool ContactUpdater::update_contacts_prives()
{
    bool result = false;
    if(does_file_exist(private_path) && reader.openReader(private_path, true))
    {
        Db db;
        if(db.connect(dbPath))
        {
            Db_template_contact contact;
            if(db.init_query(Query_Type::INSERT, contact))
            {
                ContactPrive c;
                bool error = false;               
                if(db.begin_transaction())
                {
                    try
                    {
                        while(reader.get_next(&c))
                        {
                            contact.update_DbInstance(&c);
                            error = !db.exec_insert(contact);
                            if(error)
                            {
                                break;
                            }
                        }
                    }
                    catch(const std::exception& e)
                    {
                        error = true;
                    }
                    
                    if(error)
                    {
                        db.rollback_transaction();
                    }
                    else
                    {
                        db.commit_transaction();
                        result = true;
                    }
                }
            }
        }
        if(result)
        {
            remove(private_path.c_str());
        }
    }
    return result;
}

void ContactUpdater::start()
{
    cout << "Démarrage du service de mise à jour de la base de données." << endl;
    pthread_create (&thread, NULL, update_contacts, this);
}

void ContactUpdater::stop()
{
    pthread_cancel(thread);
    void *result;
    pthread_join(thread, &result);
    cout << "Arrêt du service de mise à jour de la base de données." << endl;
}

void* update_contacts(void* arg)
{
    ContactUpdater* updater = (ContactUpdater*)arg;
     // activer les signaux d'annulation
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    // annuler le thread de manière asynchrone
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    while(true)
    {
        updater->update_contacts_prives();
        updater->update_contacts_pros();
        sleep(UPDATE_DELAY_SECONDS);
    }

    return 0;
}

