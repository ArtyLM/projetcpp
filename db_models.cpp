#include "db_models.hpp"


Db_template_contact::Db_template_contact():Db_template(), model(NULL)
{
    this->set_name("CONTACTS");
    this->add_keyProperty("IdContact");
    this->add_property("Nom");
    this->add_property("Prenom");
    this->add_property("Sexe");
    this->add_property("Entreprise");
    this->add_property("rue");
    this->add_property("Complement");
    this->add_property("cp");
    this->add_property("Ville");
    this->add_property("mail");
    this->add_property("dtNaissance");
    
}

void Db_template_contact::read_from_stmt(sqlite3_stmt* stmt)
{
    int i=0;
    this->instance.setIdContact(sqlite3_column_int(stmt, i++));
    this->instance.setNom(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setPrenom(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setSexe(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setEntreprise(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setRue(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setComplement(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setCp(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setVille(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setMail(( char*)sqlite3_column_text(stmt, i++));
    this->instance.setDtNaissance(( char*)sqlite3_column_text(stmt, i++));
    this->update_model();
}

void Db_template_contact::bind_to_stmt(sqlite3_stmt* stmt, bool is_update)
{
    int i=1;
    if(!is_update)
    {
        if(instance.getIdContact()>=0)
        {
            sqlite3_bind_int(stmt, i++, instance.getIdContact());
        }
        else
        {
            sqlite3_bind_null(stmt, i++);
        }
        
    }
    sqlite3_bind_text(stmt, i++, this->instance.getNom(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getPrenom(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getSexe(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getEntreprise(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getRue(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getComplement(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getCp(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getVille(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getMail(), -1, 0);
    sqlite3_bind_text(stmt, i++, this->instance.getDtNaissance(), -1, 0);

}

void set_addresse(Adresse& adresse, const Db_contact& contact)
{
    adresse.Setrue(contact.getRue());
    adresse.Setcp(contact.getCp());
    adresse.Setville(contact.getVille());
}

void set_commonProperties(Contact* c, const Db_contact& contact)
{
    c->SetidContact(contact.getIdContact());
    c->Setnom(contact.getNom());
    c->Setprenom(contact.getPrenom());
    c->Setsexe(contact.getSexe());
}

Date Db_Date_to_model(const char* d)
{
        string s = string(d);
        string year = s.substr(0, 4);
        string month = s.substr(5,2);
        string day = s.substr(8,2);
        Date result;
        result.Setannee(stoi(year));
        result.Setmois(stoi(month));
        result.Setjour(stoi(day));
        return result;
}

void from_model_to_db(const Contact* c, Db_contact& instance)
{
    instance.setIdContact((int)c->GetidContact());
    instance.setNom(c->Getnom());
    instance.setPrenom(c->Getprenom());
    instance.setSexe(c->Getsexe());

    const ContactPrive* cpriv = dynamic_cast<const ContactPrive*>(c);
    if(cpriv != NULL)
    {
        from_model_to_db(cpriv, instance);
    }
    else
    {
        const ContactPro* cpro = dynamic_cast<const ContactPro*>(c);
        if(cpro != NULL)
        {
            from_model_to_db(cpro, instance);
        }
    }
}

void from_model_to_db(const ContactPrive* c, Db_contact& instance)
{
    char dt[11];
    Date d = c->GetdtNaissance();
    sprintf(dt, "%04d-%02d-%02d", d.Getannee(), d.Getmois(), d.Getjour());
    instance.setDtNaissance(dt);
    Adresse a = c->Getadresse();
    instance.setRue(a.Getrue());
    instance.setComplement(a.Getcomplement());
    instance.setCp(a.Getcp());
    instance.setVille(a.Getville());
}

void from_model_to_db(const ContactPro* c, Db_contact& instance)
{
    instance.setEntreprise(c->Getentreprise());
    instance.setMail(c->Getmail());
    Adresse a = c->GetadresseEntreprise();
    instance.setRue(a.Getrue());
    instance.setComplement(a.Getcomplement());
    instance.setCp(a.Getcp());
    instance.setVille(a.Getville());
}

Contact* from_db_to_model(Db_contact& instance)
{
    Contact* result = NULL;
    if(instance.getEntreprise() != NULL)
    {
        ContactPro* c = new ContactPro();
        c->Setentreprise(instance.getEntreprise());
        c->Setmail(instance.getMail());
        Adresse a = c->GetadresseEntreprise();
        set_addresse(a, instance);
        c->SetadresseEntreprise(a);
        result = c;
    }
    else if(instance.getDtNaissance() != NULL)
    {
        ContactPrive* c = new ContactPrive();
        c->SetdtNaissance(Db_Date_to_model(instance.getDtNaissance()));
        Adresse a = c->Getadresse();
        set_addresse(a, instance);
        c->Setadresse(a);
        result = c;
    }
    else
    {
        result = new Contact();
    }

    set_commonProperties(result, instance);
    return result;
}

void Db_template_contact::update_model()
{
    this->clean_model();
    this->model = from_db_to_model(this->instance);
}

void Db_template_contact::update_DbInstance(const Contact* c)
{
    from_model_to_db(c, this->instance);
}


string Db_template_contact::to_string()
{
    stringstream ss;
    ss << this->model->infos();
    return ss.str();
}


void Db_template_contact::clean_model()
{
    if(this->model != NULL)
    {
        free(this->model);
         this->model=NULL;
    }
}



string build_query_select(const Db_template& db_obj, const char* condition)
{
    stringstream ss;
    ss << "SELECT ";
    int i=0;
    int last_i = db_obj.get_keyProperties().size() -1;
    for(string key: db_obj.get_keyProperties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i || db_obj.get_properties().size()>0)
        {
            ss << ", ";
        }
    }
    last_i = db_obj.get_properties().size() - 1;
    i=0;
    for(string key: db_obj.get_properties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i)
        {
            ss << ", ";
        }
    }
    ss << " FROM " << to_safe_field(db_obj.get_name());

    if(condition != NULL)
    {
        ss << " WHERE " << condition;
    }
    ss << ";";

    return ss.str();
}

string build_query_insert(const Db_template& db_obj)
{
    stringstream ss;
    ss << "INSERT OR IGNORE INTO " << to_safe_field(db_obj.get_name()) << "(";
    int i=0;
    int last_i = db_obj.get_keyProperties().size() -1;
    for(string key: db_obj.get_keyProperties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i || db_obj.get_properties().size()>0)
        {
            ss << ", ";
        }
    }
    last_i = db_obj.get_properties().size() -1;
    i = 0;
    for(string key: db_obj.get_properties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i)
        {
            ss << ", ";
        }
    }
    ss << ") VALUES(";
    i=0;
    last_i = db_obj.get_keyProperties().size() -1;
    for(string key: db_obj.get_keyProperties())
    {
        ss << "?";
        if(i++ != last_i || db_obj.get_properties().size()>0)
        {
            ss << ", ";
        }
    }
    i=0;
    last_i = db_obj.get_properties().size() -1;
    for(string prop : db_obj.get_properties())
    {
        ss << "?";
        if(i++ != last_i)
        {
            ss << ", ";
        }
    }
    ss << ");";
    return ss.str();
}

string build_query_delete(const Db_template& db_obj, const char* condition)
{
    stringstream ss;
    ss << "DELETE FROM " << to_safe_field(db_obj.get_name());
    if(condition != NULL) ss << " WHERE " << condition;
    ss << ";";
    return ss.str();
}

string build_query_update(const Db_template& db_obj, const char* condition)
{
    stringstream ss;
    ss << "UPDATE " << to_safe_field(db_obj.get_name()) << " SET ";
    int i=0;
    int last_i = db_obj.get_properties().size() -1;
    for(string prop : db_obj.get_properties())
    {
        ss << to_safe_field(prop) << " = ?";
        if(i++ != last_i)
        {
            ss << ", ";
        }
        
    }
    if(condition != NULL)
    {
        ss << " WHERE " << condition;
    }
    ss << ";";
    return ss.str();
}

string to_safe_field(string s)
{
    stringstream ss;
    ss << "[" << s << "]";
    return ss.str();
}
