#pragma once

#include <fstream>
#include <iostream>
#include <vector>
#include "contactprive.hpp"
#include "contactpro.hpp"

using namespace std;

class ContactReader
{
    public:
        ContactReader(){}
        bool openReader(string path, bool skip_header=true);
        bool get_next(ContactPrive* c);
        bool get_next(ContactPro* c);
        vector<ContactPrive> read_all_privates();
        vector<ContactPro> read_all_pros();
    private:
        ifstream reader;
};

void line_to_Contact(ContactPrive& c, vector<string>& l);
void handle_carriage_return(vector<string>& row);
int get_value_or_default(string value, int defaultValue);
const char* get_value_or_default(string value, const char* defaultValue);
Date get_value_or_default(string value, Date defaultValue);
int line_to_Contact_commons(Contact& c, vector<string> l);
void line_to_Contact(ContactPrive& c, vector<string>& l);
void line_to_Contact(ContactPro& c, vector<string>& l);