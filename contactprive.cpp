#include "contactprive.hpp"

ContactPrive::ContactPrive()
{
    //ctor
}

ContactPrive::~ContactPrive()
{
    //dtor
}


string ContactPrive::infos() const
{
    stringstream ss;
    ss << Contact::infos() << endl;
    ss << this->adresse.infos() << endl;
    ss << "Anniversaire: " << this->dtNaissance.infos();
    return ss.str();
}
