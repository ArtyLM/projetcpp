#ifndef SQLITE_IO_HPP_INCLUDED
#define SQLITE_IO_HPP_INCLUDED

#include "db_models.hpp"

typedef enum Query_Type{SELECT, INSERT, UPDATE, DELETE} Query_Type;

class Db
{
    public:
    Db();
    ~Db();

    bool connect(string path);
    bool init_query(Query_Type, Db_template& o, const char* condition = NULL);
    bool begin_transaction();
    bool commit_transaction();
    bool rollback_transaction();
    bool get_next(Db_template& o);
    bool exec_insert(Db_template& o);
    bool exec_delete();
    bool exec_update(Db_template& o);
    void finalize_query();
    void disconnect();
    private:
        bool execute_query(const char* query);
        string path;
        sqlite3* dbo;
        sqlite3_stmt* stmt;
        Query_Type query_type;
};


#endif // SQLITE_IO_HPP_INCLUDED
