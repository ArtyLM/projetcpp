#include "adresse.hpp"

Adresse::Adresse(): rue(NULL), complement(NULL), cp(NULL), ville(NULL)
{
    //ctor
}

Adresse::Adresse(const Adresse& a): Adresse()
{
    clean_copy(&this->rue, a.Getrue());
    clean_copy(&this->complement, a.Getcomplement());
    clean_copy(&this->cp, a.Getcp());
    clean_copy(&this->ville, a.Getville());
}

void Adresse::operator=(const Adresse& a)
{
    clean_copy(&this->rue, a.Getrue());
    clean_copy(&this->complement, a.Getcomplement());
    clean_copy(&this->cp, a.Getcp());
    clean_copy(&this->ville, a.Getville());
}

Adresse::~Adresse()
{
    clean_string(&this->rue);
    clean_string(&this->complement);
    clean_string(&this->cp);
    clean_string(&this->ville);
}

void Adresse::Setrue(const char* val)
{
    clean_copy(&this->rue, val);
}

void Adresse::Setcomplement(const char* val)
{
    clean_copy(&this->complement, val);
}

void Adresse::Setcp(const char* val)
{
    clean_copy(&this->cp, val);
}

void Adresse::Setville(const char* val)
{
    clean_copy(&this->ville, val);
}

string Adresse::infos() const
{
    stringstream ss;
    if(this->rue != NULL) ss << this->Getrue();
    if(this->complement!=NULL) ss << " ," << this->Getcomplement();
    if(this->cp != NULL) ss << ", " << this->Getcp();
    if(this->ville != NULL) ss << " " << this ->Getville();
    return ss.str();
}

ostream& operator<<(ostream& stream, const Adresse& a)
{
    stream << a.infos();
    return stream;
}

