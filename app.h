#ifndef APP_H
#define APP_H
#include "test.h"
#include "sqlite_io.hpp"
#include "contactprive.hpp"
#include "contactpro.hpp"
#include "validator.hpp"
#include "contactUpdater.hpp"

typedef enum critere{VILLE, NOM} critere;

class app
{
    public:
        app();
        void init();
    private:
        void afficheMenu();
        void selection_action();
        void ajouterContact();
        void rechercheContact();
        void supprimerContact();
        void listeContact(const char* condition);
        void quitter();
        ContactUpdater updater;
};

extern Validator validator;

#endif // APP_H
