#include "contactpro.hpp"


ContactPro::ContactPro(): entreprise(NULL), mail(NULL)
{
    //ctor
}

ContactPro::ContactPro(const ContactPro& c): ContactPro()
{
    this->adresseEntreprise = c.adresseEntreprise;
    clean_copy(&this->entreprise, c.Getentreprise());
    clean_copy(&this->mail, c.Getmail());
}

ContactPro::~ContactPro()
{
    clean_string(&this->entreprise);
    clean_string(&this->mail);
}

void ContactPro::operator=(const ContactPro& c)
{
    this->adresseEntreprise = c.adresseEntreprise;
    clean_copy(&this->entreprise, c.Getentreprise());
    clean_copy(&this->mail, c.Getmail());
}



void ContactPro::Setentreprise(const char* val)
{
    clean_string(&this->entreprise);
    if(val != NULL)
    {
        int len = (int)strlen(val);
        this->entreprise = new char[len+1];
        for(int i=0;i<len;i++)
        {
            this->entreprise[i] = toupper(val[i]);
        }
        this->entreprise[len]='\0';
    }
}

void ContactPro::Setmail(const char* val)
{
    clean_copy(&this->mail, val);
}

string ContactPro::infos() const
{
    stringstream ss;
    ss << Contact::infos() << endl;
    if(this->mail != NULL)
    {
        ss << this->mail << endl;
    }
    if(this->entreprise != NULL) ss << this->entreprise << ", " << this->adresseEntreprise.infos();
    return ss.str();
}

