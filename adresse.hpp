#ifndef ADRESSE_HPP
#define ADRESSE_HPP

#include "commons.hpp"


class Adresse
{
    public:
        Adresse();
        Adresse(const Adresse&);
        virtual ~Adresse();
        char* Getrue() const { return rue; }
        void Setrue(const char* val);
        char* Getcomplement() const { return complement; }
        void Setcomplement(const char* val);
        char* Getcp() const { return cp; }
        void Setcp(const char* val);
        void operator=(const Adresse&);
        char* Getville() const { return ville; }
        void Setville(const char* val);
        string infos() const;

    protected:

    private:
        char* rue;
        char* complement;
        char* cp;
        char* ville;
};

ostream& operator<<(ostream& stream, Adresse& a);

#endif // ADRESSE_HPP
