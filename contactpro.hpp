#ifndef CONTACTPRO_HPP
#define CONTACTPRO_HPP

#include "contact.hpp"
#include "adresse.hpp"
#include "commons.hpp"


class ContactPro : public Contact
{
    public:
        ContactPro();
        ContactPro(const ContactPro&);
        ~ContactPro();
        void operator=(const ContactPro&);
        char* Getentreprise() const { return entreprise; }
        void Setentreprise(const char* val);
        Adresse GetadresseEntreprise() const { return adresseEntreprise; }
        void SetadresseEntreprise(Adresse val) {adresseEntreprise = val;};
        char* Getmail() const { return mail; }
        void Setmail(const char* val);
        string infos() const override;

    protected:

    private:
        char* entreprise;
        Adresse adresseEntreprise;
        char* mail;
};

#endif // CONTACTPRO_HPP
