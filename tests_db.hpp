#ifndef TESTS_DB_HPP_INCLUDED
#define TESTS_DB_HPP_INCLUDED

#include "sqlite_io.hpp"
#include "contactprive.hpp"
#include "contactpro.hpp"

ContactPrive build_contact_prive();

ContactPro build_contact_pro();

void test_contact();

void test_db_select(const char* condition = NULL);

void test_db_insert();

void test_db_update();

void test_db_delete();

#endif