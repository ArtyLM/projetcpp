#ifndef DB_MODELS_HPP_INCLUDED
#define DB_MODELS_HPP_INCLUDED


#include <iostream>
#include <vector>
#include <sstream>
#include <sqlite3.h>
#include "contactprive.hpp"
#include "contactpro.hpp"

using namespace std;

class Db_template
{
    public:
        Db_template(){}
        ~Db_template(){};
        vector<string> get_properties() const {return db_properties;};
        vector<string> get_keyProperties() const {return db_keyProperties;};
        string get_name() const {return db_name;}
        virtual void read_from_stmt(sqlite3_stmt* stmt)=0;
        virtual void bind_to_stmt(sqlite3_stmt* stmt, bool is_update)=0;
        virtual void clean_model() = 0;
        virtual void update_model() = 0;
        virtual string to_string() = 0;
    protected:
        void set_name(string name){db_name = name;}
        void add_property(string p){db_properties.push_back(p);}
        void add_keyProperty(string p){db_keyProperties.push_back(p);}
    private:
        string db_name;
        vector<string> db_properties;
        vector<string> db_keyProperties;
};

typedef class Db_contact
{
    public:
        Db_contact():idContact(-1), nom(NULL),prenom(NULL), sexe(NULL), entreprise(NULL), rue(NULL), 
                    complement(NULL), cp(NULL), ville(NULL), mail(NULL),dtNaissance(NULL){}
        Db_contact(const Db_contact& c): Db_contact()
        {
            this->idContact = c.idContact;
            clean_copy(&nom, c.nom);
            clean_copy(&prenom, c.prenom);
            clean_copy(&sexe, c.sexe);
            clean_copy(&entreprise, c.entreprise);
            clean_copy(&rue, c.rue);
            clean_copy(&complement, c.complement);
            clean_copy(&cp, c.cp);
            clean_copy(&ville, c.ville);
            clean_copy(&mail, c.mail);
            clean_copy(&dtNaissance, c.dtNaissance);
        }
        ~Db_contact()
        {
            clean_string(&nom);
            clean_string(&prenom);
            clean_string(&sexe);
            clean_string(&entreprise);
            clean_string(&rue);
            clean_string(&complement);
            clean_string(&cp);
            clean_string(&ville);
            clean_string(&mail);
            clean_string(&dtNaissance);
        }
        int getIdContact() const{return idContact;}
        char* getNom() const{return nom;}
        char* getPrenom() const{return prenom;}
        char* getSexe() const{return sexe;}
        char* getEntreprise() const{return entreprise;}
        char* getRue() const {return rue;};
        char* getComplement() const{return complement;}
        char* getCp() const{return cp;};
        char* getVille() const{return ville;}
        char* getMail() const{return mail;}
        char* getDtNaissance() const {return dtNaissance;}
        void setIdContact(int val){idContact = val;}
        void setNom(char * val){clean_copy(&nom, val);}
        void setPrenom(char * val){clean_copy(&prenom, val);}
        void setSexe(char * val){clean_copy(&sexe, val);}
        void setEntreprise(char * val){clean_copy(&entreprise, val);}
        void setRue(char * val){clean_copy(&rue, val);}
        void setComplement(char * val){clean_copy(&complement, val);}
        void setCp(char * val){clean_copy(&cp, val);}
        void setVille(char * val){clean_copy(&ville, val);}
        void setMail(char * val){clean_copy(&mail, val);}
        void setDtNaissance(char * val){clean_copy(&dtNaissance, val);}
    private:
        int idContact;
        char* nom;
        char* prenom;
        char* sexe;
        char* entreprise;
        char* rue;
        char* complement;
        char* cp;
        char* ville;
        char* mail;
        char* dtNaissance;
} Db_contact;

class Db_template_contact: public Db_template
{
    public:
        Db_template_contact();
        ~Db_template_contact(){this->clean_model();};
        void read_from_stmt(sqlite3_stmt* stmt) override;
        void bind_to_stmt(sqlite3_stmt* stmt, bool is_update) override;
        const Contact& getModel(){return *model;}
        const Db_contact& getDbInstance(){return instance;};
        void update_model() override;
        void update_DbInstance(const Contact* c);
        string to_string() override;
        void clean_model() override;
    private:

        Db_contact instance;
        Contact* model;

};

string to_safe_field(string s);
void set_addresse(Adresse& adresse, const Db_contact& contact);
string build_query_select(const Db_template& db_obj, const char* condition);
string build_query_insert(const Db_template& db_obj);
string build_query_delete(const Db_template& db_obj, const char* condition);
string build_query_update(const Db_template& db_obj, const char* condition);
void set_addresse(Adresse& adresse, const Db_contact& contact);
void set_commonProperties(Contact* c, const Db_contact& contact);
void from_model_to_db(const Contact* c, Db_contact& instance);
void from_model_to_db(const ContactPrive* c, Db_contact& instance);
void from_model_to_db(const ContactPro* c, Db_contact& instance);
Contact* from_db_to_model(Db_contact& instance);
void Date_to_db(const Date& d, char** db);
Date Db_Date_to_model(const char* d);

#endif // DB_MODELS_HPP_INCLUDED
