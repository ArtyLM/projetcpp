#include "commons.hpp"

void clean_string(char** s)
{
    if(s != NULL)
    {
        free(*s);
        *s = NULL;
    }
}

void clean_copy(char** dest, const char* source)
{
    clean_string(dest);
    if(source != NULL)
    {
        *dest = new char[strlen(source) + 1];
        strcpy(*dest, source);
    }
}

bool does_file_exist(string path)
{
    ifstream f(path);
    return f.good();
}


