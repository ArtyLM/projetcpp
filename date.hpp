#ifndef DATE_HPP
#define DATE_HPP

#include "commons.hpp"

class Date
{
    public:
        Date();
        virtual ~Date();

        unsigned short Getjour() const { return jour; }
        void Setjour(unsigned short val) { jour = val; }
        unsigned short Getmois() const { return mois; }
        void Setmois(unsigned short val) { mois = val; }
        unsigned short Getannee() const { return annee; }
        void Setannee(unsigned short val) { annee = val; }
        string infos() const;

    protected:

    private:
        unsigned short jour;
        unsigned short mois;
        unsigned short annee;
};

ostream& operator<<(ostream& stream, Date& d);

#endif // DATE_HPP
