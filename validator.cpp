#include "validator.hpp"

Validator validator;

char Validator::getValidChar(const char* validChars, int nbChars)
{
    char c;
    cin >> c;
    bool valid = false;
    do
    {
        char lowerc = tolower(c);
        for(int i=0;i<nbChars;i++)
        {
            if(lowerc == tolower(validChars[i]))
            {
                valid = true;
                break;
            }
        }
        if(!valid)
        {
            cout << "Erreur de saisie veuillez recommencer: ";
            cin.ignore(numeric_limits<streamsize>::max(),'\n');
            cin >> c;
        }
    } while (valid == false);
    return c;
}

string Validator::getValidString_Length(int maxLength)
{
    string s;
    getline(cin, s);
    bool valid = false;
    do
    {
        valid = s.length()<= maxLength;
        if(!valid)
        {
            cout << "La chaîne doit faire aux maximum " << maxLength << " caractères." << endl;
            cout << "Veuillez recommencer: ";
            cin.ignore(numeric_limits<streamsize>::max(),'\n');
            getline(cin, s);
        }
    } while (!valid);
    return s;
}

int Validator::getNumber()
{
    int x = 0;
    while(!(cin >> x)){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << "Veuillez saisir un nombre: ";
    }
    return x;
}

int Validator::getNumber(int valMin, int valMax)
{
    int x = 0;
    while(!(cin >> x)){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << "Veuillez saisir un nombre: ";
    }
    if(x < valMin || x > valMax)
    {
        cout << "La valeur est hor de la plage: ";
        return getNumber(valMin, valMax);
    }
    
    return x;
}

string Validator::getValidString_Contain_Char(char c)
{
    string s;
    getline(cin, s);
    bool valid = false;
    do
    {
        valid = s.find(c) != string::npos;
        if(!valid)
        {
            cout << "La chaîne doit contenir le caractère " << c << "." << endl;
            cout << "Veuillez recommencer: ";
            getline(cin, s);
        }
    } while (!valid);
    return s;
}
