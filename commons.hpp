#ifndef COMMONS_HPP_INCLUDED
#define COMMONS_HPP_INCLUDED

#define UPDATE_DELAY_SECONDS 10

#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

void clean_string(char** s);
void clean_copy(char** dest, const char* source);
bool does_file_exist(string paht);

const string dbPath = "../dbContacts.db";
const string private_path = "../ressources/newprivates.csv";
const string pro_path = "../ressources/newprofs.csv";

#endif // COMMONS_HPP_INCLUDED
