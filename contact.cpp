#include "contact.hpp"

Contact::Contact():idContact(-1), nom(NULL), prenom(NULL), sexe(NULL)
{
    //ctor
}

Contact::Contact(const Contact& c): Contact()
{
    this->idContact = c.idContact;
    clean_copy(&this->nom, c.Getnom());
    clean_copy(&this->prenom, c.Getprenom());
    clean_copy(&this->sexe, c.Getsexe());
}

Contact::~Contact()
{
    clean_string(&this->nom);
    clean_string(&this->prenom);
    clean_string(&this->sexe);
}

void Contact::operator=(const Contact& c)
{
    this->idContact = c.idContact;
    clean_copy(&this->nom, c.Getnom());
    clean_copy(&this->prenom, c.Getprenom());
    clean_copy(&this->sexe, c.Getsexe());
}


void Contact::Setnom(const char* val)
{
    clean_string(&this->nom);
    if(val != NULL)
    {
        int len = (int)strlen(val);
        this->nom = new char[len+1];
        for(int i=0;i<len;i++)
        {
            this->nom[i] = toupper(val[i]);
        }
        this->nom[len]='\0';
    }

}

void Contact::Setprenom(const char* val)
{
    clean_string(&this->prenom);
    if(val != NULL)
    {
        int len = (int)strlen(val);
        this->prenom = new char[len+1];
        this->prenom[0] = toupper(val[0]);
        for(int i=1;i<len;i++)
        {
            this->prenom[i] = tolower(val[i]);
        }
        this->prenom[len]='\0';
    }
}

void Contact::Setsexe(const char* val)
{
    clean_string(&this->sexe);
    if(val != NULL)
    {
        this->sexe = new char[2];
        this->sexe[0]= toupper(val[0]);
        this->sexe[1] = '\0';
    }
}

string Contact::infos() const
{
    stringstream ss;
    ss << this->Getprenom() << " " << this->Getnom() << ", " << this->Getsexe();
    return ss.str();
}

ostream& operator<<(ostream& stream, Contact& c)
{
    stream << c.infos();
    return stream;
}


