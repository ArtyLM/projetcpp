#include "tests_db.hpp"

ContactPrive build_contact_prive()
{
    ContactPrive c;
    c.Setprenom("mathias");
    c.Setnom("tiberghien");
    c.Setsexe("M");
    Adresse a;
    a.Setrue("87, bd Chanzy");
    a.Setcp("93100");
    a.Setville("Montreuil");
    c.Setadresse(a);
    Date d;
    d.Setannee(1975);
    d.Setmois(1);
    d.Setjour(23);
    c.SetdtNaissance(d);
    return c;
}

ContactPro build_contact_pro()
{
    ContactPro cp;
    cp.Setprenom("melanie");
    cp.Setnom("lenvieux");
    cp.Setsexe("F");
    cp.Setmail("melanie.lenvieux@google.fr");
    cp.Setentreprise("Google");
    Adresse a;
    a.Setrue("118, Bd Lenoir");
    a.Setcp("75014");
    a.Setville("Paris");
    cp.SetadresseEntreprise(a);
    return cp;
}

void test_contact()
{
    ContactPrive c = build_contact_prive();
    ContactPro cp = build_contact_pro();
    cout << "Contact Privé:" << endl << c << endl ;
    cout << "--------------------" << endl;
    cout << "Contact Pro:" << endl << cp << endl;
}

void test_db_select(const char* condition)
{
    Db db;
    if(db.connect("../dbContacts.db"))
    {
        cout << "connection ok" << endl;
        Db_template_contact contact;
        string query = build_query_select(contact, condition);
        cout << "test query: "  << query << endl;
        if (db.init_query(SELECT, contact, condition))
        {
            while(db.get_next(contact))
            {
                cout << "----------------" << endl;
                cout << "Id: " << contact.getDbInstance().getIdContact() << endl;
                cout << contact.getModel().infos() << endl;
            }
        }
        db.finalize_query();
        db.disconnect();
    }
}

void test_db_insert()
{
    Db db;
    if(db.connect("../dbContacts.db"))
    {
        Db_template_contact contact;
        ContactPrive c = build_contact_prive();
        ContactPro cp = build_contact_pro();
        string query = build_query_insert(contact);
        cout << "query insert: " << query << endl;
        if(db.init_query(INSERT, contact, NULL))
        {
            contact.update_DbInstance(&c);
            if(db.exec_insert(contact))
            {
                cout << "Ajout d'un contact privé ok" << endl;
            }
            ;
            contact.update_DbInstance(&cp);
            if(db.exec_insert(contact))
            {
                cout << "Ajout d'un contact pro ok" << endl;
            }
        }
        db.finalize_query();
        db.disconnect();
    }

}

void test_db_update()
{
    Db db;
    if(db.connect("../dbContacts.db"))
    {
        Db_template_contact contact;
        ContactPro cp = build_contact_pro();
        cp.Setnom("lemieux");
        cp.Setmail("mlemieux@google.fr");
        const char* condition = "Nom = 'LENVIEUX'";
        string query = build_query_update(contact, condition);
        cout << "query update: " << query << endl;
        if(db.init_query(UPDATE, contact, condition))
        {
            contact.update_DbInstance(&cp);
            if(db.exec_update(contact))
            {
                cout << "Update contact ok" << endl;
            }
        }
        db.finalize_query();
        db.disconnect();
    }

}

void test_db_delete()
{
    Db db;
    if(db.connect("../dbContacts.db"))
    {
        Db_template_contact contact;
        const char* condition = "idContact > 11";
        string query = build_query_delete(contact, condition);
        cout << "query delete: " << query << endl;
        if(db.init_query(DELETE, contact, condition))
        {
            if(db.exec_delete())
            {
                cout << "delete query ok" << endl;
            }
        }
        db.finalize_query();
        db.disconnect();
    }
}